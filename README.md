RESTful API to display facebook comments and posts 
==============================

### The functionality of this project is to display facebook posts and comments when provided with a valid facebook url of form:
### "https://www.facebook.com/page_name" or "https://www.facebook.com/page_name/posts/post_id"


* tests may be run with tox

* Lesson in security (don't leave security tokens lying around in your code)

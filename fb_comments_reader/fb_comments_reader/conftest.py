from django.conf import settings
from django.core.wsgi import get_wsgi_application


def pytest_configure():
    settings.configure(REST_FRAMEWORK={'UNAUTHENTICATED_USER': None},
                       ROOT_URLCONF='fb_comments_reader.urls')
    application = get_wsgi_application()



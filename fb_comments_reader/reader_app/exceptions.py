from contextlib import contextmanager

from facebook import GraphAPIError
from rest_framework.exceptions import APIException


@contextmanager
def graph_api_error_handler():
    try:
        yield
    except GraphAPIError:
        raise APIException(detail='Invalid page or post data provided in the url')

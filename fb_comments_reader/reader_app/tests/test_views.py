from django.urls import reverse
from rest_framework.test import APIRequestFactory

from reader_app import views


class TestFacebookCommentsView:
    factory = APIRequestFactory()
    endpoint = reverse('get-fb-comments')

    def test_view_with_page_name_and_post_id_success(self):
        request_data = {"url": "https://www.facebook.com/Buzzcapture/posts/1295249200515073"}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.FacebookCommentsView.as_view()(request)
        assert response.status_code == 200
        assert response.data['page']
        assert len(response.data['posts']) == 1
        assert len(response.data['posts'][0]['comments']) == 4

    def test_view_with_page_name_and_wrong_post_id_fail(self):
        request_data = {"url": "https://www.facebook.com/Buzzcapture/posts/129524"}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.FacebookCommentsView.as_view()(request)
        assert response.status_code == 500
        assert response.data['detail']

    def test_view_with_page_name_only_success(self):
        request_data = {"url": "https://www.facebook.com/Buzzcapture"}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.FacebookCommentsView.as_view()(request)
        assert response.status_code == 200
        assert response.data['page']
        assert len(response.data['posts']) == 10

    def test_view_with_wrong_page_name_fail(self):
        request_data = {"url": "https://www.facebook.com/Buzz"}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.FacebookCommentsView.as_view()(request)
        assert response.status_code == 500
        assert response.data['detail']

    def test_view_no_page_data_fail(self):
        request_data = {"url": "https://www.facebook.com"}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.FacebookCommentsView.as_view()(request)
        assert response.status_code == 404
        assert response.data['detail']

    def test_invalid_url_input_fail(self):
        request_data = {"url": "facebook.com"}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.FacebookCommentsView.as_view()(request)
        assert response.status_code == 400
        assert response.data['url']

    def test_empty_url_input_fail(self):
        request_data = {"url": ""}
        request = self.factory.post(self.endpoint, request_data, format='json')
        response = views.FacebookCommentsView.as_view()(request)
        assert response.status_code == 400
        assert response.data['url']

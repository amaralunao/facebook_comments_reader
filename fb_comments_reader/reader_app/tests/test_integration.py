import pytest
from rest_framework.exceptions import APIException, NotFound
from rest_framework.response import Response

from reader_app import models, serializers


class TestIntegration:

    def test_get_comments_with_page_name_and_post_id_success(self):
        request_data = {"url": "https://www.facebook.com/Buzzcapture/posts/1295249200515073"}
        input_serializer = serializers.InputSerializer(data=request_data)
        assert input_serializer.is_valid()
        output_serializer = serializers.OutputSerializer(models.FacebookComments(
                                                        **input_serializer.validated_data).get_output())
        response = Response(output_serializer.data)
        assert response.status_code == 200
        assert response.data['page']
        assert len(response.data['posts']) == 1
        assert len(response.data['posts'][0]['comments']) == 4

    def test_get_comments_with_page_name_only_success(self):
        request_data = {"url": "https://www.facebook.com/Buzzcapture"}
        input_serializer = serializers.InputSerializer(data=request_data)
        assert input_serializer.is_valid()
        output_serializer = serializers.OutputSerializer(models.FacebookComments(
                                                        **input_serializer.validated_data).get_output())
        response = Response(output_serializer.data)
        assert response.status_code == 200
        assert response.data['page']
        assert len(response.data['posts']) == 10

    def test_get_comments_no_data_fail(self):
        request_data = {"url": "https://www.facebook.com"}
        input_serializer = serializers.InputSerializer(data=request_data)
        assert input_serializer.is_valid()
        with pytest.raises(NotFound):
            serializers.OutputSerializer(models.FacebookComments(
                                         **input_serializer.validated_data).get_output())

    def test_get_comments_wrong_page_name_fail(self):
        request_data = {"url": "https://www.facebook.com/Buzz"}
        input_serializer = serializers.InputSerializer(data=request_data)
        assert input_serializer.is_valid()
        with pytest.raises(APIException):
            serializers.OutputSerializer(models.FacebookComments(
                                         **input_serializer.validated_data).get_output())

import pytest
from rest_framework.test import APIClient

from reader_app import models


@pytest.fixture
def dummy_page_data():
    return {'id': '133531496686855',
            'name': 'Buzzcapture'}


@pytest.fixture
def dummy_commenter_data():
    return {'id': '1468622993229948',
            'name': 'YorikDeGraaf'}


@pytest.fixture
def dummy_comment_data():
    return {
            'created_time': '2016-09-07T20:20:14+0000',
            'message': 'Leuk Nick Harberink',
            'from': {'name': 'Bas Knikhuis',
                     'id': '1468622993229948'},
            'id': '1295249200515073_1295860870453906'
    }


@pytest.fixture
def dummy_post_data():
    return {
            'created_time': '2017-09-09T10:20:19+0000',
            'message': 'Lees hier alles over de 7 pijlers van online',
            'id': '133531496686855_1777160532323935'
    }


@pytest.fixture
def dummy_page(dummy_page_data):
    return models.Page(dummy_page_data)


@pytest.fixture
def dummy_commenter(dummy_commenter_data):
    return models.Commenter(dummy_commenter_data)


@pytest.fixture
def dummy_comment(dummy_comment_data):
    return models.Comment(dummy_comment_data)


@pytest.fixture
def dummy_post(dummy_post_data, dummy_comment_data):
    return models.Post(dummy_post_data, comments=[dummy_comment_data])


@pytest.fixture
def dummy_output(dummy_page, dummy_post):
    return models.Output({'page': dummy_page,
                          'posts': [dummy_post]})



@pytest.fixture(scope='function')
def api_client(request):
    return APIClient()

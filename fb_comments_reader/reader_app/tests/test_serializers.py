import json

import pytest

from reader_app import serializers


class TestInputSerializer:

    def test_input_serializer_success(self):
        serializer = serializers.InputSerializer(
                            data={'url':
                                  'https://www.facebook.com/Buzzcapture'})
        assert serializer.is_valid()

    def test_input_serializer_fail(self):
        serializer = serializers.InputSerializer(
                            data={'url':
                                  'www.facebook.com/Buzzcapture'})
        assert serializer.is_valid() is False
        assert serializer.errors['url']


class TestPageSerializer:

    def test_serializer_data(self, dummy_page):
        serializer = serializers.PageSerializer(dummy_page)
        serializer_data = json.loads(json.dumps(serializer.data))
        assert serializer.data['name'] == 'Buzzcapture'
        assert serializer.data['id'] == '133531496686855'


class TestCommenterSerializer:

    def test_serializer_data(self, dummy_commenter):
        serializer = serializers.CommenterSerializer(dummy_commenter)
        serializer_data = json.loads(json.dumps(serializer.data))
        assert serializer.data['name'] == 'YorikDeGraaf'
        assert serializer.data['id'] == '1468622993229948'


class TestCommentSerializer:

    def test_serializer_data(self, dummy_comment):
        serializer = serializers.CommentSerializer(dummy_comment)
        serializer_data = json.loads(json.dumps(serializer.data))
        assert serializer.data['message'] == 'Leuk Nick Harberink'
        assert serializer.data['created_time'] == '2016-09-07T20:20:14+0000'
        assert serializer.data['id'] == '1295249200515073_1295860870453906'
        assert serializer.data['commenter']['id'] == '1468622993229948'
        assert serializer.data['commenter']['name'] == 'Bas Knikhuis'


class TestPostSerializer:

    def test_serializer_data(self, dummy_post):
        serializer = serializers.PostSerializer(dummy_post)
        serializer_data = json.loads(json.dumps(serializer.data))
        assert serializer.data['message'] == 'Lees hier alles over de 7 pijlers van online'
        assert serializer.data['created_time'] == '2017-09-09T10:20:19+0000'
        assert serializer.data['id'] == '133531496686855_1777160532323935'
        assert serializer.data['comments'][0]['id'] == '1295249200515073_1295860870453906'
        assert serializer.data['comments'][0]['message'] == 'Leuk Nick Harberink'


class TestOutputSerializer:

    def test_serializer_data(self, dummy_output):
        serializer = serializers.OutputSerializer(dummy_output)
        serializer_data = json.loads(json.dumps(serializer.data))
        assert serializer.data['page']['id'] == '133531496686855'
        assert serializer.data['page']['name'] == 'Buzzcapture'
        assert len(serializer.data['posts']) == 1
        assert serializer.data['posts'][0]['id'] == '133531496686855_1777160532323935'

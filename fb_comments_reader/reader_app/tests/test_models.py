from unittest import TestCase
from unittest.mock import MagicMock, patch

import pytest
from facebook import GraphAPIError
from rest_framework.exceptions import APIException, NotFound

from reader_app import models


class TestDictObject:

    def test_args_converted_to_properties_on_initialization(self):
        args = (("key1", 1), ("key2", 2))
        obj = models.DictObject(args)

        assert obj.__dict__ == dict(args)
        assert obj.key1 == 1
        assert obj.key2 == 2

    def test_kwargs_converted_to_properties_on_initialization(self):
        kwargs = dict(key1=1, key2=2)
        obj = models.DictObject(**kwargs)

        assert obj.__dict__ == dict(kwargs)
        assert obj.key1 == 1
        assert obj.key2 == 2

    def test_invalid_parameters_are_ignored(self):
        """
        `1` cannot be converted to a dict which will cause a TypeError which
        is passed in the DictObject __init__ method.
        """
        try:
            models.DictObject(1)
        except TypeError:
            self.fail("DictObject raised an unexpected TypeError")

    def test_raises_error_when_attribute_does_not_exist(self):
        """
        When a non-existent DictObject instance method is called
        and no attribute or method with that name
        can be found an AttributeError should be raised
        """
        obj = models.DictObject()
        with pytest.raises(AttributeError):
            obj.get_doesnotexist()

    def test_can_get_attribute_using_get_method(self):
        test_value = "rabbit"
        obj = models.DictObject(dict(animal=test_value))
        assert obj.get('animal') == test_value


class TestPage:

    def test_initialize_page_success(self, dummy_page_data):
        model = models.Page(dummy_page_data)
        assert model.get_id() == dummy_page_data['id']
        assert model.get_name() == dummy_page_data['name']

    def test_empty_data(self):
        data = {}
        model = models.Page(data)
        assert model.get_id() is None
        assert model.get_name() is None


class TestCommenter:

    def test_initialize_commenter_success(self, dummy_commenter_data):
        model = models.Commenter(dummy_commenter_data)
        assert model.get_id() == dummy_commenter_data['id']
        assert model.get_name() == dummy_commenter_data['name']

    def test_empty_data(self):
        data = {}
        model = models.Commenter(data)
        assert model.get_id() is None
        assert model.get_name() is None


class TestComment:

    def test_initialize_comment_success(self, dummy_comment_data):
        model = models.Comment(dummy_comment_data)
        assert model.get_id() == dummy_comment_data['id']
        assert model.get_created_time() == dummy_comment_data['created_time']
        assert model.get_message() == dummy_comment_data['message']
        assert isinstance(model.get_commenter(), models.Commenter)

    def test_empty_data(self):
        data = {}
        model = models.Comment(data)
        assert model.get_id() is None
        assert model.get_created_time() is None
        assert model.get_message() is None
        assert isinstance(model.get_commenter(), models.Commenter)


class TestPost:

    def test_initialize_post_success(self, dummy_post_data, dummy_comment_data):
        model = models.Post(dummy_post_data, comments=[dummy_comment_data])
        assert model.get_id() == dummy_post_data['id']
        assert model.get_created_time() == dummy_post_data['created_time']
        assert model.get_message() == dummy_post_data['message']
        assert isinstance(list(model.get_comments())[0], models.Comment)

    def test_empty_data(self):
        data = {}
        model = models.Post(data, comments=[])
        assert model.get_id() is None
        assert model.get_created_time() is None
        assert model.get_message() is None
        assert len(list(model.get_comments())) == 0


class TestOutput:

    def test_output_initialization_success(self, dummy_page, dummy_post):
        data = {'page': dummy_page, 'posts': [dummy_post]}
        model = models.Output(data)
        assert isinstance(model.get_page(), models.Page)
        assert isinstance(model.get_posts()[0], models.Post)

    def test_empty_data(self):
        data = {}
        model = models.Output(data)
        assert model.get_page() is None
        assert model.get_posts() is None


class TestFacebookComments:

    def test_parse_url_page_id_and_post_id_success(self):
        url = 'https://www.facebook.com/Buzzcapture/posts/1295249200515073'
        model = models.FacebookComments(url=url)
        page_name, post_id = model._parse_url()
        assert page_name == 'Buzzcapture'
        assert post_id == '1295249200515073'
        assert model.page_name == page_name
        assert model.post_id == post_id

    def test_parse_url_missing_post_id_success(self):
        url = 'https://www.facebook.com/Buzzcapture/'
        model = models.FacebookComments(url=url)
        page_name, post_id = model._parse_url()
        assert page_name == 'Buzzcapture'
        assert post_id is None
        assert model.page_name == page_name
        assert model.post_id == post_id

    def test_parse_url_indexerror(self):
        url = 'https://www.facebook.com'
        model = models.FacebookComments(url=url)
        page_name, post_id = model._parse_url()
        assert page_name is None
        assert post_id is None
        assert model.page_name == page_name
        assert model.post_id == post_id

    def test_get_page_instance_success(self, dummy_page_data):
        mock_graph = MagicMock()
        mock_graph.get_object.return_value = dummy_page_data
        model = models.FacebookComments(url='')
        model.graph = mock_graph
        page_instance = model._get_page_instance('Buzzcapture')
        assert isinstance(page_instance, models.Page)
        assert page_instance.get_id() == '133531496686855'

    def test_get_page_instance_raises_exception(self, dummy_page_data):
        mock_graph = MagicMock()
        mock_graph.get_object.side_effect = GraphAPIError(result='')
        model = models.FacebookComments(url='')
        model.graph = mock_graph
        with pytest.raises(APIException):
            page_instance = model._get_page_instance('Buzzcapture')

    def test_get_comments_success(self, dummy_comment_data):
        mock_graph = MagicMock()
        resp = {'paging':
                {'cursors':
                    {'after': u'WTI5dGJXVnVkRjlqZAFhKemIzSTZANVEk1TlRrMRXgZD',
                     'before': u'WTI5dGJXVnVkRjlqZAFhKemIzSTZANVEk1TlRnUZD'}},
                'data': [dummy_comment_data, dummy_comment_data],
                }
        mock_graph.get_connections.return_value = resp
        model = models.FacebookComments(url='')
        model.graph = mock_graph
        comments = model._get_comments('some_id')
        assert comments == resp['data']

    def test_get_comments_raises_exception(self, dummy_comment_data):
        mock_graph = MagicMock()
        mock_graph.get_connections.side_effect = GraphAPIError(result='')
        model = models.FacebookComments(url='')
        model.graph = mock_graph
        with pytest.raises(APIException):
            comments = model._get_comments('some_id')


    def test_get_posts_list_success(self, dummy_post_data):
        mock_graph = MagicMock()
        resp = {
                "data": [dummy_post_data, dummy_post_data]
        }
        mock_graph.get_connections.return_value = resp
        model = models.FacebookComments(url='')
        model.graph = mock_graph
        posts_list = model._get_posts_list('some_id')
        assert len(posts_list) == 2
        assert isinstance(posts_list[0], models.Post)

    def test_get_posts_list_raises_exception(self, dummy_post_data):
        mock_graph = MagicMock()
        mock_graph.get_connections.side_effect = GraphAPIError(result='')
        model = models.FacebookComments(url='')
        model.graph = mock_graph
        with pytest.raises(APIException):
            posts_list = model._get_posts_list('some_id')


    @patch('reader_app.models.FacebookComments._get_comments')
    def test_get_post_instance(self, mock_method,
                               dummy_post_data,
                               dummy_comment_data):

        mock_method.return_value = [dummy_comment_data, dummy_comment_data]
        model = models.FacebookComments(url='')
        post = model._get_post_instance(dummy_post_data)
        assert isinstance(post, models.Post)
        assert len(list(post.get_comments())) == 2

    def test_get_post_by_id_success(self, dummy_post_data):
        page = models.Page(dict(id='133531496686855'))
        post_id = '1777160532323935'

        mock_graph = MagicMock()
        mock_graph.get_object.return_value = dummy_post_data
        model = models.FacebookComments(url='')
        model.graph = mock_graph

        post = model._get_post_by_id(page, post_id)
        assert post == dummy_post_data
        mock_graph.get_object.assert_called_with(fields='id,created_time,message',
                                                 id='133531496686855_1777160532323935')

    def test_get_post_by_id_success(self, dummy_post_data):
        page = models.Page(dict(id='133531496686855'))
        post_id = '1777160532323935'

        mock_graph = MagicMock()
        mock_graph.get_object.side_effect = GraphAPIError(result='')
        model = models.FacebookComments(url='')
        model.graph = mock_graph
        with pytest.raises(APIException):
            post = model._get_post_by_id(page, post_id)

    @patch('reader_app.models.FacebookComments._get_posts_list')
    @patch('reader_app.models.FacebookComments._get_page_instance')
    @patch('reader_app.models.FacebookComments._parse_url')
    def test_get_output_only_page_name_success(self,
                                               mock_parse_url,
                                               mock_page_instance,
                                               mock_posts_list,
                                               dummy_page,
                                               dummy_post_data):

        mock_parse_url.return_value = 'Buzzcapture', None
        mock_page_instance.return_value = dummy_page
        mock_posts_list.return_value = [dummy_post_data, dummy_post_data]

        model = models.FacebookComments(url='')
        output = model.get_output()

        assert isinstance(output, models.Output)
        assert isinstance(output.get_page(), models.Page)
        assert isinstance(output.get_posts(), list)
        assert len(output.get_posts()) == 2

    @patch('reader_app.models.FacebookComments._get_post_by_id')
    @patch('reader_app.models.FacebookComments._get_page_instance')
    @patch('reader_app.models.FacebookComments._parse_url')
    def test_get_output_page_name_and_post_id_success(self,
                                                      mock_parse_url,
                                                      mock_page_instance,
                                                      mock_post_by_id,
                                                      dummy_page,
                                                      dummy_post_data):
        mock_parse_url.return_value = 'Buzzcapture', '1295249200515073'
        mock_page_instance.return_value = dummy_page
        mock_post_by_id.return_value = dummy_post_data

        model = models.FacebookComments(url='')
        output = model.get_output()

        assert isinstance(output, models.Output)
        assert isinstance(output.get_page(), models.Page)
        assert isinstance(output.get_posts(), list)
        assert len(output.get_posts()) == 1

    @patch('reader_app.models.FacebookComments._parse_url')
    def test_get_output_pabe_name_exception(self,
                                            mock_parse_url):
        mock_parse_url.return_value = None, None
        model = models.FacebookComments(url='')
        with pytest.raises(NotFound):
            model.get_output()

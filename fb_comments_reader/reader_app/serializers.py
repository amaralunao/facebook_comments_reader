from rest_framework import serializers


class InputSerializer(serializers.Serializer):
    url = serializers.URLField()


class PageSerializer(serializers.Serializer):
    id = serializers.CharField(source='get_id')
    name = serializers.CharField(source='get_name')


class CommenterSerializer(serializers.Serializer):
    id = serializers.CharField(source='get_id')
    name = serializers.CharField(source='get_name')


class CommentSerializer(serializers.Serializer):
    id = serializers.CharField(source='get_id')
    created_time = serializers.DateTimeField(source='get_created_time')
    message = serializers.CharField(source='get_message')
    commenter = CommenterSerializer(source='get_commenter')


class PostSerializer(serializers.Serializer):
    id = serializers.CharField(source='get_id')
    created_time = serializers.DateTimeField(source='get_created_time')
    message = serializers.CharField(source='get_message')
    comments = serializers.ListField(child=CommentSerializer(),
                                     source='get_comments')


class OutputSerializer(serializers.Serializer):
    page = PageSerializer(source='get_page')
    posts = serializers.ListField(child=PostSerializer(), source='get_posts')

from django.conf.urls import url

from reader_app import views

urlpatterns = [
    url(r'^$', views.FacebookCommentsView.as_view(), name='get-fb-comments'),
]

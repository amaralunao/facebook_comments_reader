from rest_framework.response import Response
from rest_framework.views import APIView

from . import models, serializers


class FacebookCommentsView(APIView):
    serializer_class = serializers.InputSerializer
    model = models.FacebookComments

    def post(self, request, format=None):
        input_serializer = self.serializer_class(data=request.data)
        input_serializer.is_valid(raise_exception=True)
        output_serializer = serializers.OutputSerializer(
            self.model(**input_serializer.validated_data).get_output())
        return Response(output_serializer.data)

import facebook
from rest_framework.exceptions import NotFound

from . import constants
from . import exceptions

class DictObject:

    def __init__(self, *args, **kwargs):
        try:
            for key, value in dict(*filter(None, args), **kwargs).items():
                self.__dict__[key] = value
        except TypeError:
            pass

    def get(self, key, default=None):
        return getattr(self, key, default)


class Page(DictObject):

    def get_id(self):
        return self.get('id')

    def get_name(self):
        return self.get('name')


class Commenter(DictObject):

    def get_id(self):
        return self.get('id')

    def get_name(self):
        return self.get('name')


class Comment(DictObject):

    def get_id(self):
        return self.get('id')

    def get_created_time(self):
        return self.get('created_time')

    def get_message(self):
        return self.get('message')

    def get_commenter(self):
        return Commenter(self.get('from'))


class Post(DictObject):

    comments = None

    def get_id(self):
        return self.get('id')

    def get_created_time(self):
        return self.get('created_time')

    def get_message(self):
        return self.get('message')

    def get_comments(self):
        if self.comments:
            for comment_entry in self.comments:
                yield Comment(comment_entry)


class Output(DictObject):

    def get_page(self):
        return self.get('page')

    def get_posts(self):
        return self.get('posts')


class FacebookComments:
    def __init__(self, url=None):
        self.url = url
        self.page_name = None
        self.post_id = None
        self.graph = facebook.GraphAPI(access_token=constants.ACCESS_TOKEN,
                                       version=2.10)

    def _parse_url(self):
        url_components = self.url.split("/")
        for index, item in enumerate(url_components):
            if item == constants.FACEBOOK_URL:
                try:
                    self.page_name = url_components[index+1]
                except IndexError:
                    pass
            if item == "posts":
                try:
                    self.post_id = url_components[index+1]
                except IndexError:
                    pass
        return self.page_name, self.post_id

    def _get_page_instance(self, page_name):
        with exceptions.graph_api_error_handler():
             page = Page(self.graph.get_object(id=page_name, fields='id,name'))
        return page

    def _get_comments(self, post_id):

        with exceptions.graph_api_error_handler():
            comments_response = self.graph.get_connections(id=post_id,
                                                           connection_name='comments',
                                                           filter='stream')
        comments = comments_response['data']
        return comments

    def _get_posts_list(self, page_id):
        posts_list = []

        with exceptions.graph_api_error_handler():
            posts_response = self.graph.get_connections(id=page_id,
                                                        connection_name='posts',
                                                        limit=10)
        posts = posts_response['data']
        for post in posts:
            post_instance = self._get_post_instance(post)
            posts_list.append(post_instance)
        return posts_list

    def _get_post_instance(self, post):
        post_id = post['id']
        comments = self._get_comments(post_id)
        return Post(post, comments=comments)

    def _get_post_by_id(self, page, post_id):
        page_id = page.get_id()
        post_id_for_lookup = '{}_{}'.format(page_id, post_id)

        with exceptions.graph_api_error_handler():
            post = self.graph.get_object(id=post_id_for_lookup,
                                         fields='id,created_time,message')
        return post

    def get_output(self):

        output = dict()
        page = None
        posts = []
        page_name, post_id = self._parse_url()

        if page_name and post_id:
            page = self._get_page_instance(page_name)
            post = self._get_post_by_id(page, post_id)
            post_instance = self._get_post_instance(post)
            posts.append(post_instance)

        elif page_name and not post_id:
            page = self._get_page_instance(page_name)
            posts = self._get_posts_list(page_name)
        else:
            raise NotFound(detail="Page name not found in url",
                           code="page_name_not_found")

        output['page'] = page
        output['posts'] = posts
        return Output(output)
